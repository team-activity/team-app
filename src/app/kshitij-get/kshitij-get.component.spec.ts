import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KshitijGetComponent } from './kshitij-get.component';

describe('KshitijGetComponent', () => {
  let component: KshitijGetComponent;
  let fixture: ComponentFixture<KshitijGetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KshitijGetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KshitijGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

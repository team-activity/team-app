import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubDeshbordComponent } from './sub-deshbord.component';

describe('SubDeshbordComponent', () => {
  let component: SubDeshbordComponent;
  let fixture: ComponentFixture<SubDeshbordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubDeshbordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubDeshbordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

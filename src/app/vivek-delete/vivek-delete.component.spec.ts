import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VivekDeleteComponent } from './vivek-delete.component';

describe('VivekDeleteComponent', () => {
  let component: VivekDeleteComponent;
  let fixture: ComponentFixture<VivekDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VivekDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VivekDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
